import React,{Component} from "react";

const getTime=(millyseconds)=>{
    const minutes = Math.trunc(millyseconds/60000);
    const seconds = Math.trunc(millyseconds/1000) - minutes*60;
    const millysecond = millyseconds - (Math.trunc(millyseconds/1000))*1000;
    return minutes + ":" + seconds + ":" + millysecond;
}

class Stopwatch extends Component{
    state={
        time:0,
        startTtime: 0,
        run:false,
        freezTime:0,
        laps:[]
    }
    timeId = null;
    handleClickStart=()=>{
        let nowTime = new Date();
        this.setState({
            run:true,
            startTtime: nowTime.getTime(),
            time: nowTime.getTime()
        });
        this.timeId = setInterval(()=>this.addMillysecond(),15);
    }
    handleClickPause=()=>{
        this.setState({
            run:false,
            freezTime: this.state.time - this.state.startTtime + this.state.freezTime, 
            time:0,
            startTtime:0
        });
        clearInterval(this.timeId);
    }
    handleClickReset=()=>{
        this.setState({
            time:0,
            startTtime: 0,
            run:false,
            freezTime:0,
            laps:[]
        });
        clearInterval(this.timeId);
    }
    addMillysecond=()=>{
        let nowTime = new Date();
        this.setState({
            time: nowTime.getTime()
        })
    }
    handleClickLap=()=>{
        this.setState({
            laps:[...this.state.laps,this.state.time-this.state.startTtime]
        })
    }
    render(){
        const timer = this.state.time - this.state.startTtime + this.state.freezTime;
        console.log(timer, this.state.time, this.state.startTtime, this.state.freezTime)
        return(
        <>
            <div>
                <div>{getTime(timer)}</div>
                {this.state.run ? <button onClick={this.handleClickPause}>Pause</button>:<button onClick={this.handleClickStart}>Start</button>}
                <button onClick={this.handleClickReset}>Reset</button>
                <button onClick={this.handleClickLap}>Lap</button>
            </div>
            <ul>{this.state.laps.map((lap)=>{
                return <li>{getTime(lap)}</li>
            })}</ul>
        </>
        );
    }
}
export default Stopwatch;