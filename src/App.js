import './App.css';
import Stopwatch from "./components/Stopwatch/Stopwatch.jsx";

function App() {
  return (
    <div><Stopwatch/></div>
  );
}

export default App;
